//
//  Cat.spec.ts
//  Tests
//
//  Created on February 02, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import Cat from "../../Cradle/Cat"

import * as chai from 'chai';
const expect = chai.expect;

describe('> Cat class', () => {
    let cat: Cat
    let olderCat: Cat

    before(() => {
        cat = new Cat({
            forename: "Dixie",
            surname: "Mason",
            dateOfBirth: "2009-02-13",
            colour: "black",
            image: {
                url: "http://25.media.tumblr.com/tumblr_m3qf8sQXfQ1qhwmnpo1_1280.jpg"
            }
        })

        olderCat = new Cat({
            forename: "Older",
            surname: "Mason",
            dateOfBirth: "2007-02-13",
            colour: "black",
            image: {
                url: "http://25.media.tumblr.com/tumblr_m3qf8sQXfQ1qhwmnpo1_1280.jpg"
            }
        })
    })

    it("Should calculate age correctly" , () => {
        expect(cat.getAge().years).to.equal(10)
    });

    it("Should calculate age gap correctly", () => {
        expect(cat.ageGapFrom(olderCat)).to.equal(-24);
    })
});