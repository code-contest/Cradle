//
//  App.ts
//  Cradle
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import Express      from "express"
import { Animal }   from "./Cradle/Animal"
import Cat          from "./Cradle/Cat"
import Dog          from "./Cradle/Dog"
import Hamster      from "./Cradle/Hamster"

let app = Express()
let port = process.env.PORT || 3000

app.get("/", async (_, response) => {
    let dogs     = await Dog.GetAll()
    let cats     = await Cat.GetAll()
    let hamsters = await Hamster.GetAll()

    if (dogs.length == 0 && cats.length == 0 && hamsters.length == 0) {
        response.status(500).end("None of the downstream APIs returned a value.")
    }

    // Sort dogs
    // The unary minus (-) sign here turns an ascending sort function to
    // a descending sort. Alternatively, one could write b.ageGapFrom(a)
    // to achieve the same objective.
    dogs.sort((a, b) => -a.ageGapFrom(b))
    dogs = dogs.map(dog => dog.export())

    // Sort cats
    let gingerCats = Array<Cat>()
    let blackCats  = Array<Cat>()
    let restCats   = Array<Cat>()
    
    cats.forEach(cat => {
        let colour = cat.getColour().toLowerCase()
        switch (colour) {
        case "ginger":
            gingerCats.push(cat)
            break
        case "black":
            blackCats.push(cat)
            break
        default:
            restCats.push(cat)
        }
    })

    gingerCats.sort((a, b) => -a.ageGapFrom(b))
    blackCats.sort((a, b) => -a.ageGapFrom(b))
    restCats.sort((a, b) => -a.ageGapFrom(b))
    
    // Safe to reset the cats array here
    cats = Array<Cat>().concat(gingerCats, blackCats, restCats)
    cats = cats.map(cat => cat.export())

    // Sort hamsters
    hamsters.sort((a, b) => a.ageGapFrom(b))
    hamsters = hamsters.map(hamster => hamster.export())
    
    // Compile all the arrays 
    let list = Array<Animal>().concat(dogs, cats, hamsters)
    let body = JSON.stringify(list, null, 2)

    response.status(200).end(body)
})

app.listen(port, () => {
    console.log(`>  Server listening at port ${port}...`)
})