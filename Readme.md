# Cradle

A Node.js REST API server written in TypeScript. The server exposes one endpoint:
`GET /`.

```
$ npm start         // To start server locally
$ npm test          // To run tests
```