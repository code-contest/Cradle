//
//  Cat.ts
//  Cradle/Cat
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import { Age, Animal } from "../Animal"
import { Fetch, CatServiceResponse } from "./CatService"

export default class Cat implements Animal {
    private dob: Date
    private imageURL: URL
    private firstName: string
    private lastName: string
    private colour: string

    constructor(cat: CatServiceResponse) {
        this.firstName  = cat.forename
        this.lastName   = cat.surname
        this.colour     = cat.colour
        this.imageURL   = new URL(cat.image.url)
        this.dob        = new Date(cat.dateOfBirth)
    }

    public getAge(): Age {
        let today  = new Date()
        let years  = today.getFullYear() - this.dob.getFullYear()
        
        let months: number
        if (today.getMonth() >= this.dob.getMonth()) {
            months = today.getMonth() - this.dob.getMonth()
        }
        else {
            // + 1 in the end, since months are 0-indexed in JavaScript Date
            months = (11 - this.dob.getMonth()) + today.getMonth() + 1
            years -= 1
        }

        return {
            months,
            years
        }
    }

    public getKind(): string {
        return "Cat"
    }

    public getFullName(): string {
        return this.firstName + " " + this.lastName
    }

    public getImageURL(): string {
        return this.imageURL.toJSON()
    }

    public getColour(): string {
        return this.colour
    }

    public export(): any {
        return {
            age: this.getAge(),
            colour: this.getColour(),
            name: this.getFullName(),
            imageURL: this.getImageURL(),
            kind: this.getKind()
        }
    }

    public ageGapFrom(other: Cat): number {
        let age = this.getAge()
        let ageInMonths = (age.years * 12) + age.months

        let otherAge = other.getAge()
        let otherAgeInMonths = (otherAge.years * 12) + otherAge.months

        return ageInMonths - otherAgeInMonths
    }

    public static async GetAll(): Promise<Array<Cat>> {
        let cats = Array<Cat>()
        let response = await Fetch()
        response.forEach(item => cats.push(new Cat(item)))
        return cats
    }
}