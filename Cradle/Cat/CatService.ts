//
//  CatService.ts
//  Cradle/Cat
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import { CallAPI }  from "../NetworkService"
type Cats = Array<CatServiceResponse>

export interface CatServiceResponse {
    forename: string,
    surname: string,
    dateOfBirth: string,
    colour: string,
    image: {
        url: string
    }
}

export async function Fetch(): Promise<Cats> {
    let options = {
        host: "apigateway.test.lifeworks.com",
        path: "/rescue-shelter-api/cats",
        method: "GET",
        headers: {
            "Content-Type": "application/json" 
        }
    }
    
    try {
        let response = await CallAPI(options)
        if (response.statusCode != 200) {
            return []
        }
        return response.data
    }
    catch {
        // Typically I will handle the error here, but given the scope
        // of the task, it is fine for now to simply ignore the error
        // and return an empty array.
        return []
    }
}
