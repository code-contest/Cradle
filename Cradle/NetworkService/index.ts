//
//  index.ts
//  Cradle/NetworkService
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import { CallAPI, NetworkResponse, } from "./NetworkService"

export { CallAPI, NetworkResponse }