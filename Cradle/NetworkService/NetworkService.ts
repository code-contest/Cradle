//
//  NetworkService.ts
//  Cradle/NetworkService
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import HTTPS from "https"

export interface NetworkResponse {
    statusCode?: number,
    data: any  
}

export function CallAPI(options: any): Promise<NetworkResponse> {
    return new Promise<NetworkResponse>((resolve, reject) => {
        let request = HTTPS.request(options, (response) => {
            let incoming = ""
            response.setEncoding('utf8');

            response.on('data', function (chunk) {
                incoming += chunk;
            });

            response.on('end', function() {
                let data = JSON.parse(incoming);
                resolve({
                    statusCode: response.statusCode,
                    data: data.body
                });
            });
        })

        request.on('error', function(error) {
            console.log(">  Network request failed. Error: ", error)
            reject(error)
        });
    
        request.end();
    })
}