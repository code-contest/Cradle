//
//  index.ts
//  Cradle/Hamster
//
//  Created on February 02, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import Hamster from "./Hamster"
export default Hamster 