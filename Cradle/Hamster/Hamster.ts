//
//  Hamster.ts
//  Cradle/Hamster
//
//  Created on February 02, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import { Age, Animal } from "../Animal"
import { Fetch, HamsterServiceResponse } from "./HamsterService"

export default class Hamster implements Animal {
    private dob: Date
    private imageURL: URL
    private firstName: string
    private lastName: string

    constructor(hamster: HamsterServiceResponse) {
        this.firstName  = hamster.forename
        this.lastName   = hamster.surname
        this.imageURL   = new URL(hamster.image.url)
        this.dob        = new Date(hamster.dateOfBirth)
    }

    public getAge(): Age {
        let today  = new Date()
        let years  = today.getFullYear() - this.dob.getFullYear()
        
        let months: number
        if (today.getMonth() >= this.dob.getMonth()) {
            months = today.getMonth() - this.dob.getMonth()
        }
        else {
            // + 1 in the end, since months are 0-indexed in JavaScript Date
            months = (11 - this.dob.getMonth()) + today.getMonth() + 1
            years -= 1
        }

        return {
            months,
            years
        }
    }

    public getKind(): string {
        return "Hamster"
    }

    public getFullName(): string {
        return this.firstName + " " + this.lastName
    }

    public getImageURL(): string {
        return this.imageURL.toJSON()
    }

    public export(): any {
        return {
            age: this.getAge(),
            name: this.getFullName(),
            imageURL: this.getImageURL(),
            kind: this.getKind()
        }
    }

    public ageGapFrom(other: Hamster): number {
        let age = this.getAge()
        let ageInMonths = (age.years * 12) + age.months

        let otherAge = other.getAge()
        let otherAgeInMonths = (otherAge.years * 12) + otherAge.months

        return ageInMonths - otherAgeInMonths
    }

    public static async GetAll(): Promise<Array<Hamster>> {
        let hamsters = Array<Hamster>()
        let response = await Fetch()
        response.forEach(item => hamsters.push(new Hamster(item)))
        return hamsters
    }
} 