//
//  HamsterService.ts
//  Cradle/Hamster
//
//  Created on February 02, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import { CallAPI }  from "../NetworkService"
type Hamsters = Array<HamsterServiceResponse>

export interface HamsterServiceResponse {
    forename: string,
    surname: string,
    dateOfBirth: string,
    image: {
        url: string
    }
}

export async function Fetch(): Promise<Hamsters> {
    let options = {
        host: "apigateway.test.lifeworks.com",
        path: "/rescue-shelter-api/hamsters",
        method: "GET",
        headers: {
            "Content-Type": "application/json" 
        }
    }
    
    try {
        let response = await CallAPI(options)
        if (response.statusCode != 200) {
            return []
        }
        return response.data
    }
    catch {
        return []
    }
}