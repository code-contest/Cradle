//
//  Dog.ts
//  Cradle/Dogs
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import { Age, Animal } from "../Animal"
import { Fetch, DogServiceResponse } from "./DogService"

export default class Dog implements Animal {
    private dob: Date
    private imageURL: URL
    private firstName: string
    private lastName: string

    constructor(dog: DogServiceResponse) {
        this.firstName  = dog.forename
        this.lastName   = dog.surname
        this.imageURL   = new URL(dog.image.url)
        this.dob        = new Date(dog.dateOfBirth)
    }

    public getAge(): Age {
        let today  = new Date()
        let years  = today.getFullYear() - this.dob.getFullYear()
        
        let months: number
        if (today.getMonth() >= this.dob.getMonth()) {
            months = today.getMonth() - this.dob.getMonth()
        }
        else {
            // + 1 in the end, since months are 0-indexed in JavaScript Date
            months = (11 - this.dob.getMonth()) + today.getMonth() + 1
            years -= 1
        }

        return {
            months,
            years
        }
    }

    public getKind(): string {
        return "Dog"
    }

    public getFullName(): string {
        return this.firstName + " " + this.lastName
    }

    public getImageURL(): string {
        return this.imageURL.toJSON()
    }

    public export(): any {
        return {
            age: this.getAge(),
            name: this.getFullName(),
            imageURL: this.getImageURL(),
            kind: this.getKind()
        }
    }

    public ageGapFrom(other: Dog): number {
        let age = this.getAge()
        let ageInMonths = (age.years * 12) + age.months

        let otherAge = other.getAge()
        let otherAgeInMonths = (otherAge.years * 12) + otherAge.months

        return ageInMonths - otherAgeInMonths
    }

    public static async GetAll(): Promise<Array<Dog>> {
        let dogs = Array<Dog>()

        let response = await Fetch()
        response.forEach(item => dogs.push(new Dog(item)))
        
        return dogs
    }
}