//
//  DogService.ts
//  Cradle/Dog
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import { CallAPI }  from "../NetworkService"
type Dogs = Array<DogServiceResponse>

export interface DogServiceResponse {
    forename: string,
    surname: string,
    dateOfBirth: string,
    image: {
        url: string
    }
}

export async function Fetch(): Promise<Dogs> {
    let options = {
        host: "apigateway.test.lifeworks.com",
        path: "/rescue-shelter-api/dogs",
        method: "GET",
        headers: {
            "Content-Type": "application/json" 
        }
    }
    
    try {
        let response = await CallAPI(options)
        if (response.statusCode != 200) {
            return []
        }
        return response.data
    }
    catch {
        return []
    }
}
