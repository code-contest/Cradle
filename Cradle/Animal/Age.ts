//
//  Age.ts
//  Cradle/Animal
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

export default interface Age {
    months: number,
    years: number
}