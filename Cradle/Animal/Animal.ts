//
//  Animal.ts
//  Cradle/Animal
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import Age  from "./Age"

export default interface Animal {
    getAge(): Age
    getColour?(): string
    getFullName(): string
    getKind(): string
    getImageURL(): string
}