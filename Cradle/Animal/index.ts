//
//  index.ts
//  Cradle/Animal
//
//  Created on February 01, 2019 by Animesh Mishra <hello@animesh.ltd>
//  © 2019 Animesh Mishra. Please read the License supplied with the software.
//

import Age      from "./Age"
import Animal   from "./Animal"
import Kind     from "./Kind"

export { Age, Animal, Kind }